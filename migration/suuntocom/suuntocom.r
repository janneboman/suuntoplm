library(stringr)
library(XML)
library(RCurl)
library(URLtools)
library(RSelenium)

# get URL list
a <- "http://www.suunto.com/en-us/product-search/see-all-sports-watches"
b <- "http://www.suunto.com/en-us/product-search/see-all-premium-watches"
c <- "http://www.suunto.com/en-us/product-search/see-all-dive-products"
d <- "http://www.suunto.com/en-us/product-search/see-all-compasses"
e <- "http://www.suunto.com/en-us/product-search/accessories"

Categorylist <- c(a, b, c, d, e)
URLpath <- c()
for (i in 1:length(Categorylist)){
page <- getURL(Categorylist[i])
parsedsuunto <- htmlParse(page, encoding = "UTL-8")
links <- unlist(xpathApply(parsedsuunto, "//a[@class=\"product-image\"]", xmlAttrs))
names(links) <- NULL
URLpath <- append(URLpath, links)
}
odd_index <- seq(1, 926, 2)
URLpath <- URLpath[odd_index]
URLparse <- url_parse(URLpath)
URLparse$domain <- "http://www.suunto.com"
URLlist <- url_compose(URLparse)
# write.csv2(URLlist, "/Users/sunsu/Documents/Pricing test/R code/CSV/SuuntoProductLinks_Janne_30.9.2016.csv", row.names = FALSE)
################################################################################################################################
URLlist <- read.csv2("/Users/sunsu/Documents/Pricing test/R code/CSV/SuuntoProductLinks_Janne_30.9.2016.csv", stringsAsFactors = FALSE)
# get content
ProductDF <- data.frame()
pageDF <- data.frame()
for (i in 1:nrow(URLlist)) {
productpage <- getURL(URLlist[351,])
parsedpage <- htmlParse(productpage)
# get SKU
SKU <- unlist(xpathApply(parsedpage,"//div[@id=\"productid\"]", xmlGetAttr, "data-ssid"))
# get production comment
Productioncomment <-unlist(xpathApply(parsedpage, "//span[@class=\"discontinued\"]", xmlValue))
if(is.null(Productioncomment)) {Productioncomment <- "Continue"}
# get product name
Name <- unlist(xpathApply(parsedpage, "//h1[@class=\"v-upper\"]", xmlValue))
# get product group
ProductGroup <- unlist(xpathApply(parsedpage, "//ul[@class=\"BreadCrumb\"]//li[2]/a", xmlValue))
# get mainbody title
MainbodyTitle <- unlist(xpathApply(parsedpage, "//div[@class=\"span7 col-sm-7\"]//h2", xmlValue))
# get mainbody content
if (is.null(MainbodyTitle)) {
  MainbodyTitle <- unlist(xpathApply(parsedpage, "//div[@class=\"span7 col-sm-7\"]//strong", xmlValue))
  MainbodyContent <- unlist(xpathApply(parsedpage, "//div[@class=\"span7 col-sm-7\"]//p", xmlValue))
}else{MainbodyContent <- unlist(xpathApply(parsedpage, "//div[@class=\"span7 col-sm-7\"]//p", xmlValue))}
if (length(MainbodyContent)>1) {MainbodyContent <- paste(MainbodyContent, collapse = "")}
if (is.null(MainbodyTitle)) {MainbodyTitle <- "No Mainbody Title"}
if (is.null(MainbodyContent)) {MainbodyContent <- "No Mainbody Content"}
# get key features
KeyFeatures <- unlist(xpathApply(parsedpage, "//div[@id=\"tab1\"]/div[@class=\"row\"]//li", xmlValue))
KeyFeatures <- paste(KeyFeatures, collapse = ";")
if (length(KeyFeatures)==0) {KeyFeatures <- (xpathApply(parsedpage, "//div[@id=\"tab1\"]/div[@class=\"row\"]/div[1]/div", xmlValue))}
KeyFeatures <- gsub("\r|\n|\t", "", KeyFeatures)
if (nchar(KeyFeatures)==0) {KeyFeatures <- "No Key Features"}
#?? get description - use RSelenium
#?? Description <- unlist(xpathApply(parsedpage, "//div[@class=\"row slogans\"]//h2", xmlValue))
# get PackageDescription
PackageDescription <- unlist(xpathApply(parsedpage, "//div[@id=\"tab1\"]//table[@class=\"feat-table\"]//td", xmlValue))
PackageDescription <- trimws(gsub("\r|\n|\t", "", PackageDescription))
if (length(PackageDescription)==0){PackageDescription <- "No Package Description"}
# get Images
Images <- unlist(xpathApply(parsedpage, "//div[@class=\"span6 col-sm-6\"]/img", xmlGetAttr, "src"))
ProductImage_src <- Images[1]
FeatureImage_src <- Images[2]
if (is.na(FeatureImage_src)) {FeatureImage_src <- "No Feature Image"}
# generate data frame
pageDF <- data.frame(ProductGroup, SKU, Name, Productioncomment, ProductImage_src, MainbodyTitle, MainbodyContent, KeyFeatures, PackageDescription, 
                     FeatureImage_src, URLlist[i,],
                     stringsAsFactors = FALSE)
ProductDF <- rbind(ProductDF, pageDF)
}
write.csv2(ProductDF, "/UserS/sunsu/Documents/Pricing test/R code/csv/SuuntoProductContent_Janne_3.10.2016.csv", row.names = FALSE)


ProductDF <- data.frame()
pageDF <- data.frame()
for (i in nchar(URLlist)) {
  productpage <- getURL(URLlist[150,])
  parsedpage <- htmlParse(productpage)
  
  # get mainbody content
  SKU <- unlist(xpathApply(parsedpage,"//div[@id=\"productid\"]", xmlGetAttr, "data-ssid"))
  MainbodyTitle <- unlist(xpathApply(parsedpage, "//div[@class=\"span7 col-sm-7\"]//h2", xmlValue))
  if (is.null(MainbodyTitle)) {
          MainbodyTitle <- unlist(xpathApply(parsedpage, "//div[@class=\"span7 col-sm-7\"]//strong", xmlValue))
          MainbodyContent <- unlist(xpathApply(parsedpage, "//div[@class=\"span7 col-sm-7\"]//p", xmlValue))[2]
  }else{MainbodyContent <- unlist(xpathApply(parsedpage, "//div[@class=\"span7 col-sm-7\"]//p", xmlValue))}
  KeyFeatures <- unlist(xpathApply(parsedpage, "//div[@id=\"tab1\"]//div[@class=\"span6  col-sm-6\"]//li", xmlValue))
  KeyFeatures <- gsub("\r|\t", "", KeyFeatures)
  KeyFeatures <- gsub("\n", ".", KeyFeatures)
    # generate data frame
  pageDF <- data.frame(SKU, MainbodyTitle, MainbodyContent)
  ProductDF <- rbind(ProductDF, pageDF)
}
ProductDF

duplicated(ProductDF$SKU)
is.null(ProductDF$MainbodyContent[12])

# get description - use RSelenium
# run java -jar selenium-server-standalone-2.53.1.jar
remDr <- remoteDriver()
remDr$open()
# 3b, download sports watches prices
remDr$navigate(URLlist[1,])
DescriptionFind <- remDr$findElement(using = "class", "span6 col-sm-6 content-area")
DescriptionHTML <- DescriptionFind$getElementAttribute("innerHTML")[[1]]
parsedDescription <- htmlParse(SWPriceHTML, encoding = "UTF-8")

remDr$close()
Description <- xpathApply(parsedpage, "//div[@class=\"row slogans\"]//h2", xmlValue)
# get image 
Image_src <- unlist(xpathApply(parsedpage, "//div[@class=\"row slogans\"]//img[@src]", xmlGetAttr, "src"))

# write.csv2(ProductDF, "/Users/sunsu/Documents/Pricing test/R code/CSV/SuuntoProductBasic_Janne_30.9.2016.csv", row.names = FALSE)
