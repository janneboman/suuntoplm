SELECT
	a.FAMILY,
	a.PLANNING_MODEL,
	b.PRODUCT_NAME,
	b.SKU_CODE,
	b.SKU_DESCRIPTION,
	b.CC_EU,
	c.TEXT1
FROM
	(SELECT DISTINCT
		FAMILY,
		PLANNING_MODEL,
		COUNT(DISTINCT(CC_EU)) AS test
	FROM [Plasma_Dev2].[dbo].[Plasma_Migration_Data_v3]
	GROUP BY FAMILY, PLANNING_MODEL
	) a
INNER JOIN
	[dbo].[Plasma_Migration_Data_v3] b
ON
	a.PLANNING_MODEL = b.PLANNING_MODEL
INNER JOIN
	(SELECT
		a.BWKEY,
		b.LAND1,
		c.[STAWN],
		c.[TEXT1]
	FROM
		[SAP_Master].dbo.[T001K] a
	INNER JOIN
		[SAP_Master].dbo.[T001] b
	ON
		a.BUKRS = b.BUKRS
	INNER JOIN
		[SAP_Master].dbo.[T604] c
	ON
		b.LAND1 = c.LAND1
	WHERE
		a.BWKEY = '5050'
	) c
ON
	b.CC_EU = c.STAWN
 WHERE a.test > 1