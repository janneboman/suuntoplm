UPDATE
	a
SET
	a.[ZZ_ARTICLE] = b.[New_Article]
	
FROM
	[SAP_Master].[dbo].[MARA] a

INNER JOIN
	[Plasma_Dev2].[dbo].[AMQ] b
ON
	a.MATNR = b.SKU

UPDATE
	a
SET
	a.[ZZ_PLANMOD] = b.[Model]

FROM
	[SAP_Master].[dbo].[MARA] a

INNER JOIN
	[Plasma_Dev2].[dbo].[AMQ] b

ON
	a.MATNR = b.[New_Article]