SELECT DISTINCT
	a.FAMILY,
	a.PRODUCT_NAME,
	a.SKU_DESCRIPTION,
	IMAGE = CASE
		WHEN b.ProductImage_src IS NULL THEN ''
		ELSE 'http://www.suunto.com' + REPLACE(b.ProductImage_src, 'width=570', 'width=100')
	END
FROM
	[Plasma_Dev2].[dbo].[Plasma_Migration_Data_v2] a
LEFT JOIN
	[Plasma_Dev2].[dbo].[Plasma_Mirgration_suuntocom] b
ON
	a.SKU_CODE = b.SKU

WHERE
	a.FAMILY IS NOT NULL
	AND a.PRODUCT_NAME IS NOT NULL
	AND a.SKU_DESCRIPTION IS NOT NULL