/* test */

SELECT
	a.MATNR,
	a.PRDHA,
	a.ERSDA,
	b.MAKTX,
	a.[Year],
	a.Comment
FROM

	(SELECT
		a.MATNR,
		a.PRDHA,
		a.ERSDA,
		b.CostingToolSKU,
		c.SKU_CODE,
		b.[Year],
		Comment = CASE
			WHEN b.CostingToolSKU IS NULL AND c.SKU_CODE IS NULL THEN 'not relevant'
			WHEN b.CostingToolSKU = c.SKU_CODE THEN 'ok'
			WHEN b.CostingToolSKU IS NULL AND c.SKU_CODE IS NOT NULL THEN 'not in costing'
			WHEN c.SKU_CODE IS NULL AND b.CostingToolSKU IS NOT NULL THEN 'not in migration'
		END
	FROM
		SAP_Master.dbo.MARA a


	LEFT JOIN
		(SELECT * FROM Pricing.dbo.CostingTool WHERE [Year] IN ('2016','2017')) b
	ON
		a.MATNR = b.CostingToolSKU

	LEFT JOIN
		Plasma_Dev2.dbo.Plasma_Migration_Data_v2 c
	ON
		a.MATNR = c.SKU_CODE

	WHERE
		a.MTART = 'FERT'
	) a

LEFT JOIN
	SAP_Master.dbo.MAKT b
ON
	a.MATNR = b.MATNR

WHERE
	a.Comment IN ('not in migration')

ORDER BY
	a.[Year] DESC,
	a.ERSDA DESC