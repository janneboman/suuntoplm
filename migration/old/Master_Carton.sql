SELECT
	'Carton ' + CAST(ROW_NUMBER() OVER(ORDER BY a.MASTER_CARTON DESC) AS VARCHAR) AS MASTER_CARTON,
	a.MASTER_CARTON AS DIMENSIONS
FROM
	(SELECT DISTINCT
		MASTER_CARTON
	FROM
		Plasma_Dev.dbo.Plasma_Migration_Data_v2
	) a
WHERE
	a.MASTER_CARTON NOT IN ('')