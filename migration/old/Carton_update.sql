INSERT

	[dbo].[Plasma_Migration_Carton2]

SELECT * FROM

	(SELECT
		a.SKU_CODE,
		b.[Komponentti] AS CARTON_CODE,
		'SU55' AS PLASMA_CODE,
		b.[Komponentin kuvaus] AS CARTON,
		'205X90X50' AS DIMENSIONS,
		'1' QTY_PER_CARTON,
		'205' AS L,
		'90' AS W,
		'50' AS H
 
	FROM
		[Plasma_Dev2].[dbo].[Plasma_Migration_Data_v2] a

	INNER JOIN
		SAP_Master.[dbo].[SQ00_BOM] b
	ON
		a.SKU_CODE = b.[Nimike ylin taso]

	WHERE
		a.MASTER_CARTON LIKE '%ESSENTIAL 7%'
		AND b.[Komponentin kuvaus] LIKE '%BOX%'



	UNION ALL

	SELECT
		a.SKU_CODE,
		b.[Komponentti] AS CARTON_CODE,
		'SU60' AS PLASMA_CODE,
		b.[Komponentin kuvaus] AS CARTON,
		'90X180X1' AS DIMENSIONS,
		'1' QTY_PER_CARTON,
		'90' AS L,
		'180' AS W,
		'1' AS H
 
	FROM
		[Plasma_Dev2].[dbo].[Plasma_Migration_Data_v2] a

	INNER JOIN
		SAP_Master.[dbo].[SQ00_BOM] b
	ON
		a.SKU_CODE = b.[Nimike ylin taso]

	WHERE
		a.MASTER_CARTON LIKE '%CLAMSHELL KB%'
		AND b.[Komponentin kuvaus] LIKE '%CLAMSHELL%'

	UNION ALL

	SELECT
		a.SKU_CODE,
		b.[Komponentti] AS CARTON_CODE,
		'SU50' AS PLASMA_CODE,
		b.[Komponentin kuvaus] AS CARTON,
		'83X18X83' AS DIMENSIONS,
		'1' QTY_PER_CARTON,
		'83' AS L,
		'18' AS W,
		'83' AS H
 
	FROM
		[Plasma_Dev2].[dbo].[Plasma_Migration_Data_v2] a

	INNER JOIN
		SAP_Master.[dbo].[SQ00_BOM] b
	ON
		a.SKU_CODE = b.[Nimike ylin taso]

	WHERE
		a.MASTER_CARTON LIKE '%CARDBOARD MANUAL%'
		AND b.[Komponentin kuvaus] LIKE '%CARDBOARD MANUAL%'

	) a