SELECT
	f.ZZ_RETAILDATE AS [RID EMEA],
	e.HERKL AS [Country of origin],
	NULL AS [Factory],
	NULL AS [Lead time producion],
	'UN' AS [UOM],
	NULL AS [Size],
	b.MATNR AS [SKU code],
	c.MAKTX AS [SKU description],
	EAN_UPC.UPC,
	EAN_UPC.EAN,
	[Gross weight] = CASE
		WHEN b.GEWEI = 'KGM' THEN b.BRGEW
	END,
	NULL AS [Master carton code],
	NULL AS [QTY/carton],
	NULL AS [Management code]
FROM
	(SELECT
		ZZ_ARTICLE,
		COUNT(MATNR) AS test
	FROM
		SAP_Master.dbo.MARA
	WHERE
		MTART = 'FERT'
	GROUP BY
		ZZ_ARTICLE
	) a
LEFT JOIN
	SAP_Master.dbo.MARA b
ON
	a.ZZ_ARTICLE = b.ZZ_ARTICLE
/* sku description */
LEFT JOIN
	SAP_Master.dbo.MAKT c
ON
	b.MATNR = c.MATNR
/*ean, upc*/
LEFT JOIN
	(SELECT DISTINCT
		a.MATNR,
		a.EAN,
		b.UPC
	FROM
		(SELECT
			MATNR,
			EAN11 AS EAN
		FROM
			SAP_Master.dbo.MEAN
		WHERE
			EANTP = 'SI'
		) a
	LEFT JOIN
		(SELECT
			MATNR,
			EAN11 AS UPC
		FROM
			SAP_Master.dbo.MEAN
		WHERE
			EANTP = 'SZ'
		) b
	ON
		a.MATNR = b.MATNR
	) EAN_UPC

ON
	b.MATNR = EAN_UPC.MATNR
/* country code*/
LEFT JOIN
	(SELECT
		MATNR,
		HERKL
	FROM
		SAP_Master.dbo.MARC
	WHERE
		WERKS = 'SUF1'
	) e
ON
	b.MATNR = e.MATNR
/* RID emea*/
LEFT JOIN
	(SELECT
		MATNR,
		ZZ_RETAILDATE
	FROM
		(SELECT
			*,
			ROW_NUMBER() OVER (PARTITION BY MATNR ORDER BY ZZ_RETAILDATE DESC) AS rn

		FROM
			SAP_Master.dbo.MVKE
		) a
	WHERE
		a.rn = 1
		AND ZZ_RETAILDATE IS NOT NULL
		AND VKORG IN
		('1100',
		'1300',
		'1400',
		'1600',
		'1700',
		'1800',
		'2100',
		'2300',
		'2400',
		'2600',
		'2640',
		'3000',
		'3300',
		'3400',
		'3500',
		'3600',
		'3700',
		'4800',
		'4900',
		'5310')
	) f
ON
	b.MATNR = f.MATNR
WHERE
	a.test = 1
	AND c.SPRAS_ISO = 'EN'
	AND LEN(c.MAKTX) <= 30
	AND f.ZZ_RETAILDATE IS NOT NULL
	AND EAN_UPC.EAN IS NOT NULL
	AND EAN_UPC.UPC IS NOT NULL