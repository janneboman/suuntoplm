INSERT INTO
	Plasma_Dev2.[dbo].[Plasma_Migration_Carton2] (SKU_CODE)

	SELECT
	a.SKU_CODE
FROM
	Plasma_Dev2.dbo.Plasma_Migration_Data_v3 a
LEFT JOIN
	Plasma_Dev2.[dbo].[Plasma_Migration_Carton2] b
ON
	a.SKU_CODE = b.SKU_CODE
WHERE
	a.MASTER_CARTON IS NULL
	AND a.FAMILY = 'MARKETING'


UPDATE
	b
SET
	b.PLASMA_CODE = 'SU40'
FROM
	Plasma_Dev2.dbo.Plasma_Migration_Data_v3 a
LEFT JOIN
	Plasma_Dev2.[dbo].[Plasma_Migration_Carton2] b
ON
	a.SKU_CODE = b.SKU_CODE
WHERE
	a.MASTER_CARTON IS NULL
	AND a.FAMILY = 'MARKETING'





SELECT
	a.SKU_CODE,
	'SU40' AS PLASMA_CODE,
	b.*
FROM
	Plasma_Dev2.dbo.Plasma_Migration_Data_v3 a
LEFT JOIN
	Plasma_Dev2.[dbo].[Plasma_Migration_Carton2] b
ON
	a.SKU_CODE = b.SKU_CODE
WHERE
	a.MASTER_CARTON IS NULL
	AND a.FAMILY = 'MARKETING'


	