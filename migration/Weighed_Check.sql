SELECT
a.SKU_CODE, a.SKU_DESCRIPTION, a.MASTER_CARTON,
b.[Material weighed]
FROM
[dbo].[Plasma_Migration_Data_v3] a
LEFT JOIN
SAP_Master.dbo.MARA_ADDITIONAL b
ON
a.SKU_CODE = b.Material
WHERE
b.[Material weighed] IS NULL
