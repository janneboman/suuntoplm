SELECT DISTINCT FAMILY, ARTICLE_TYPE, COUNT(SKU_CODE) AS SKU_Count, SKU_plan_relevance, Article_plan_relevance, Model_plan_relevance
FROM [Plasma_Dev2].[dbo].[Plasma_Migration_Data_v3]
WHERE General_Comment LIKE '%BU%'
GROUP BY FAMILY, ARTICLE_TYPE, SKU_plan_relevance, Article_plan_relevance, Model_plan_relevance
/*
SELECT * FROM Plasma_Dev2.dbo.Plasma_Migration_Data_v3 WHERE FAMILY = 'PERFORMANCE' AND ARTICLE_TYPE = 'ST' AND SKU_plan_relevance = 'A'

AND Article_plan_relevance IS NULL AND Model_plan_relevance IS NULL*/

SELECT DISTINCT FAMILY, ARTICLE_TYPE, COUNT(SKU_CODE) AS SKU_Count, Article_comment, Model_comment
FROM [Plasma_Dev2].[dbo].[Plasma_Migration_Data_v3]
WHERE General_Comment LIKE '%BU%' /*AND ( Article_comment NOT LIKE 'Clean articles' OR Model_comment NOT LIKE 'Clean Models' )*/
GROUP BY FAMILY, ARTICLE_TYPE, Article_comment, Model_comment
ORDER BY Article_comment DESC

SELECT *
FROM [Plasma_Dev2].[dbo].[Plasma_Migration_Data_v3]
WHERE Article_comment IN ('Check!') /*AND Article_plan_relevance NOT IN ('A') AND SKU_DESCRIPTION LIKE '%DUMMY%' */
ORDER BY FAMILY DESC

SELECT *
FROM [Plasma_Dev2].[dbo].[Plasma_Migration_Data_v3]
WHERE FAMILY = 'OUTDOOR' AND ARTICLE_TYPE = 'ST' AND Article_comment IS NULL AND Model_comment = 'Generated model'