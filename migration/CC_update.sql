SELECT DISTINCT
	*
FROM

	(SELECT
		a.*,
		c.MATNR,
		c.PRDHA,
		d.STAWN,
		Test = CASE
			WHEN a.[EU Tariff code] = d.STAWN THEN 'OK'
			ELSE 'NOT OK'
		END
	FROM
		[dbo].[Customs_Cleanup] a
	INNER JOIN
		SAP_Master.dbo.MARA b
	INNER JOIN
		SAP_Master.dbo.MARA c
	ON
		b.MATNR = c.ZZ_ARTICLE
	INNER JOIN
		SAP_Master.dbo.MARC d
	ON
		c.MATNR = d.MATNR
	ON
		a.[Planning code] = b.ZZ_PLANMOD
	WHERE

		d.WERKS IN (
			'1240',
			'1100',
			'1199',
			'1300',
			'1399',
			'1600',
			'1699',
			'1700',
			'1799',
			'1800',
			'1899',
			'2300',
			'2399',
			'2400',
			'2499',
			'2600',
			'2697',
			'2699',
			'3000',
			'3099',
			'3300',
			'3399',
			'3400',
			'3499',
			'3500',
			'3599',
			'3700',
			'3701',
			'3799',
			'4800',
			'4899',
			'4900',
			'4999',
			'5050',
			'5099',
			'5310',
			'5399')

	) a

WHERE


	a.Test = 'NOT OK'
	AND a.[EU Tariff code] NOT IN ('9026202090', '8517620090')