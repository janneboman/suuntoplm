USE
	[Plasma_Dev3]
GO

/* ----------------- main query ------------------ */

/* migration fields */

DELETE
	Plasma_Dev3.dbo.Plasma_Migration_Data_v3
INSERT
	Plasma_Dev3.dbo.Plasma_Migration_Data_v3

SELECT DISTINCT
	'SU' AS PRODUCT_LINE,
	'AL17' AS CURRENT_SEASON,
	NULL AS PLANNING_MODEL,
	CAST('00' AS VARCHAR) AS SPORT_CATEGORY,
	NULL AS PROJECT,
	NULL AS FAMILY,
	NULL AS PLAN_MODEL_NAME,
	NULL AS BRIEF,
	'U' AS GENDER,
	a.PRDHA AS PROD_HIERARCHY,
	NULL AS SIZE_TABLE,
	NULL AS LABEL_TABLE,
	NULL AS PRODUCT_TYPE,
	NULL AS PROD_TYPE_DESCR,
	NULL AS CUSTOMS_PRODTYPE,
	NULL AS CC_US,
	NULL AS CC_EU,
	NULL AS CC_JP,
	NULL AS CC_KO,
	NULL AS CC_CA,
	NULL AS PLANNING_ARTICLE,
	NULL AS ARTICLE_DESCRIPTION,
	NULL AS ARTICLE_TYPE,
	NULL AS COLOR_CODE,
	NULL AS COLOR_CODE2,
	NULL AS COLOR_CODE3,
	NULL AS RID_EMEA,
	NULL AS ORIGIN,
	NULL AS FACTORY,
	'0' AS LEAD_TIME, /* needs to be checked */
	'UN' AS UOM,
	NULL AS SIZE,
	a.MATNR AS SKU_CODE,
	NULL AS SKU_DESCRIPTION,
	NULL AS UPC,
	NULL AS EAN,
	NULL AS GROSS_WEIGHT,
	NULL AS MASTER_CARTON,
	'1' AS QTY_PER_CARTON,
	'S&OP - Regular Prod. 51' AS MANAGEMENT_CODE,
	NULL AS CREATED_SEASON,
	NULL AS DEVELOPMENT_TYPE,
	NULL AS SRP_EMEA,
	NULL AS SRP_US,
	NULL AS SRP_APAC,
	'SKUs loaded, MSTAV = ' + CASE WHEN a.MSTAV IS NULL THEN 'NULL' ELSE a.MSTAV END AS Comment,
	'MSTAE=' + a.MSTAE + ', MSTAV=' + a.MSTAV AS SKU_Comment,
	NULL AS Article_Comment,
	NULL AS Model_Comment,
	NULL AS SKU_plan_relevance,
	NULL AS Article_plan_relevance,
	NULL AS Model_plan_relevance,
	NULL AS General_Comment

/* end migration fields */

FROM
	(SELECT
		*
	FROM
		SAP_Master.dbo.MARA a
	LEFT JOIN
		SAP_Master.dbo.MARA_ADDITIONAL b
	ON
		a.MATNR = b.Material
	WHERE
		LEFT (b.[CURRENT Season], 4) IN ('2017', '2018')
		/* b.[CURRENT Season] IS NULL */
		AND a.MSTAE IS NULL
		AND a.MTART = 'FERT'
	) a

DELETE
	a
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	SAP_Master.dbo.MARA b
ON
	a.SKU_CODE = b.MATNR
WHERE
	b.MSTAV IN ('91', '98')

/*
DELETE
	a
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	SAP_Master.dbo.MARA_ADDITIONAL b
ON
	a.SKU_CODE = b.Material
WHERE
	b.[Usage] IN ('SL', 'PR') */

DELETE
	a
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	SAP_Master.dbo.MAKT b
ON
	a.SKU_CODE = b.MATNR
WHERE
	b.MAKTX LIKE '%#%'

/* activity tracker/watch 

DELETE FROM
	[dbo].[Plasma_Migration_Data_v3]
WHERE
	SKU_CODE IN

	('SS023122000',
	'SS023123000',
	'SS023124000',
	'SS023125000',
	'SS023126000',
	'SS023127000',
	'SS023120000',
	'SS023121000') */


;

/* ----------------- end main query ------------------ */

/* add missing SKUs to Manual_Mapping */

INSERT
	[dbo].[Plasma_Migration_Manual_Mapping] (SKU_CODE)

	SELECT
		a.SKU_CODE
	FROM
		[dbo].[Plasma_Migration_Data_v3] a
	LEFT JOIN
		[dbo].[Plasma_Migration_Manual_Mapping] b
	ON
		a.SKU_CODE = b.SKU_CODE
	WHERE
		b.SKU_CODE IS NULL

/* -------------- update some global fields ---------------------------- */

USE
	[Plasma_Dev3]
GO

/* sku description */

UPDATE
	a
SET
	a.SKU_DESCRIPTION = b.MAKTX
FROM
	dbo.Plasma_Migration_Data_v3 a
INNER JOIN
	SAP_Master.dbo.MAKT b
ON
	a.SKU_CODE COLLATE DATABASE_DEFAULT = b.MATNR COLLATE DATABASE_DEFAULT
WHERE
	b.SPRAS_ISO = 'EN'

/* family using item_category + prodh for the rest + trick for the rest .... */

UPDATE
	a
SET
	a.FAMILY = CASE
		WHEN a.Item_Category IN

		('SPARE LINKS',
		'STRAP',
		'STEEL STRAP',
		'RUBBER STRAP',
		'TEXTILE STRAP',
		'LEATHER STRAP',
		'EXTENSION STRAP',
		'BATTERY KIT',
		'BRACELET KIT',
		'BOOT',
		'DISPLAY SHIELD',
		'SCRATCH GUARD',
		'BUNGEE ADAPTER',
		'BIKE ADAPTER',
		'POWER CABLE',
		'DISPLAY SHIELD',
		'MAGNIFIER',
		'INTERFACE USB CABLE',
		'MAGNETIC USB CABLE',
		'BELT KIT',
		'POUCH',
		'BRACELET',
		'INSTRUCTION COMPASS',
		'COMPASS CASE',
		'PUNCHES',
		'CONTROL MARKER',
		'INSTRUMENT BODY COVER',
		'SERVICE KIT',
		'SLEEVE',
		'BELT',
		'MODULE',
		'POLYWATCH',
		'REPLACEMENT BATTERY',
		'HP HOSE',
		'USB INTERFACE',
		'QUICK DISCONNECT')
		AND a.Usage = 'ST' THEN
			CASE
				WHEN b.PRDHA LIKE '%D%' THEN 'DIVE'
				WHEN b.PRDHA LIKE '%T%' THEN 'PERFORMANCE'
				WHEN b.PRDHA LIKE '%W%' THEN 'OUTDOOR'
				WHEN b.PRDHA LIKE '%0A0S0A10%' THEN 'Mechanical instrument'
				WHEN b.PRDHA LIKE '%0A0S0A20%' THEN 'WATCHES'
			END

		WHEN a.Item_Category IN

		('DUMMY',
		'BANNER',
		'INFO CENTER',
		'DISPLAY',
		'KOTF',
		'ICE CUBE',
		'TABLE TALKER',
		'FLOOR STAND',
		'BROCHURE',
		'COUNTERTOP')
		AND a.Usage = 'ST' THEN 'MARKETING'

		WHEN a.Item_Category IN

		('SERVICE MODULE',
		'SERVICE KIT')
		AND a.Usage = 'SP' THEN 'SERVICE'
	END
FROM
	[dbo].[Plasma_Migration_Products] a
LEFT JOIN
	SAP_Master.dbo.MARA b
ON
	a.MATNR = b.MATNR
WHERE
	b.ZZ_ARTICLE = 'SA000182+'
	OR b.ZZ_ARTICLE = 'SA000183+'
	OR b.ZZ_ARTICLE = 'SA000184+'
	OR b.ZZ_ARTICLE = 'SA000185+'
	OR b.ZZ_ARTICLE = 'SA000515+'
	OR b.ZZ_ARTICLE IS NULL

UPDATE
	a
SET
	a.FAMILY = b.FAMILY
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	[dbo].[Plasma_Migration_Products] b
ON
	a.SKU_CODE = b.MATNR

UPDATE
	a
SET
	a.FAMILY = CASE
		WHEN a.PROD_HIERARCHY LIKE '%T%' THEN 'PERFORMANCE'
		WHEN a.PROD_HIERARCHY LIKE '%D%' THEN 'DIVE'
		WHEN RIGHT(LEFT(a.PROD_HIERARCHY, 8), 2) IN ('0W') THEN 'OUTDOOR'
		WHEN RIGHT(LEFT(a.PROD_HIERARCHY, 8), 2) IN ('20') THEN 'WATCHES'
		WHEN RIGHT(LEFT(a.PROD_HIERARCHY, 8), 2) IN ('0M', '10') THEN 'Mechanical instrument'
	END
FROM
	[dbo].[Plasma_Migration_Data_v3] a
LEFT JOIN
	SAP_Master.dbo.MARA b
ON
	a.SKU_CODE = b.MATNR
WHERE
	a.FAMILY IS NULL
	AND ( b.ZZ_ARTICLE NOT IN ('SA000182+',	'SA000183+', 'SA000184+', 'SA000185+', 'SA000515+') OR b.ZZ_ARTICLE IS NULL )

UPDATE
	a
SET
	a.FAMILY = CASE
		WHEN a.PLAN_MODEL_NAME LIKE '%ACTIVITY WATCH%' OR a.PLAN_MODEL_NAME LIKE '%ACTIVITY TRACKER%' THEN 'PERFORMANCE'
		WHEN a.PLAN_MODEL_NAME LIKE '%APEX%' OR a.PLAN_MODEL_NAME LIKE '%RIDGE%' THEN 'OUTDOOR'
		WHEN a.PLAN_MODEL_NAME = 'GUIDING STAR' THEN 'Mechanical instrument'
		ELSE a.FAMILY
	END
FROM
	[dbo].[Plasma_Migration_Products] a
WHERE
	a.FAMILY IS NULL
	AND a.Item_Category = ''

/* missing family assignments using prodh */

UPDATE
	[dbo].[Plasma_Migration_Data_v3]
SET
	FAMILY = CASE
		WHEN [PROD_HIERARCHY] IN 
			('0A0S0A0D901010',
			'0A0S0A0M901010',
			'0A0S0A0T901010',
			'0A0S0A0W901010',
			'0A0S0A10901010',
			'0A0S0A20201010')
		THEN 'ACC & Spare'
	END
WHERE
	FAMILY IS NULL
	

/* ---------------- end Family... */

/* color code */

UPDATE
	a
SET
	a.COLOR_CODE = b.COLOR_CODE
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	[dbo].[Plasma_Migration_Manual_Mapping] b
ON
	a.SKU_CODE = b.SKU_CODE

/* ----------- RID EMEA, Commodity codes, origin, EAN/UPC ----------- */

UPDATE
	a
SET
	a.RID_EMEA = b.ZZ_RETAILDATE,
	a.CC_US = e.CC_US,
	a.CC_EU = c.CC_EU,
	a.CC_KO = d.CC_KO,
	a.CC_JP = f.CC_JP,
	a.CC_CA = g.CC_CA,
	a.ORIGIN = h.HERKL,
	a.EAN = i.EAN,
	a.UPC = i.UPC

FROM
	dbo.Plasma_Migration_Data_v3 a

/* RID emea */

LEFT JOIN

	(SELECT
		*
	FROM
		(SELECT
			*,
			ROW_NUMBER() OVER (PARTITION BY MATNR ORDER BY ZZ_RETAILDATE DESC) AS rn

		FROM
			SAP_Master.dbo.MVKE
		WHERE
			VKORG IN
			('1100',
			'1300',
			'1400',
			'1600',
			'1700',
			'1800',
			'2100',
			'2300',
			'2400',
			'2600',
			'2640',
			'3000',
			'3300',
			'3400',
			'3500',
			'3600',
			'3700',
			'4800',
			'4900',
			'5310',
			'5000',
			'3410',
			'4110')

		) a
	WHERE
		a.rn = 1
		AND ZZ_RETAILDATE IS NOT NULL
	) b

ON
	a.SKU_CODE COLLATE DATABASE_DEFAULT = b.MATNR COLLATE DATABASE_DEFAULT

/* commodity codes.. */

LEFT JOIN
	(SELECT
		MATNR,
		STAWN AS CC_EU
	FROM
		SAP_Master.dbo.MARC
	WHERE
		WERKS = '5050'
	) c

ON
	a.SKU_CODE COLLATE DATABASE_DEFAULT	= c.MATNR COLLATE DATABASE_DEFAULT

LEFT JOIN
	(SELECT
		MATNR,
		STAWN AS CC_KO
	FROM
		SAP_Master.dbo.MARC
	WHERE
		WERKS = '6500'
	) d

ON
	a.SKU_CODE COLLATE DATABASE_DEFAULT	= d.MATNR COLLATE DATABASE_DEFAULT

LEFT JOIN
	(SELECT
		MATNR,
		STAWN AS CC_US
	FROM
		SAP_Master.dbo.MARC
	WHERE
		WERKS = '8800'
	) e

ON
	a.SKU_CODE COLLATE DATABASE_DEFAULT	= e.MATNR COLLATE DATABASE_DEFAULT

LEFT JOIN
	(SELECT
		MATNR,
		STAWN AS CC_JP
	FROM
		SAP_Master.dbo.MARC
	WHERE
		WERKS = '6100'
	) f

ON
	a.SKU_CODE COLLATE DATABASE_DEFAULT	= f.MATNR COLLATE DATABASE_DEFAULT

LEFT JOIN
	(SELECT
		MATNR,
		STAWN AS CC_CA
	FROM
		SAP_Master.dbo.MARC
	WHERE
		WERKS = '9100'
	) g

ON
	a.SKU_CODE COLLATE DATABASE_DEFAULT	= g.MATNR COLLATE DATABASE_DEFAULT

LEFT JOIN

	(SELECT
		MATNR,
		HERKL
	FROM
		SAP_Master.dbo.MARC
	WHERE
		WERKS = '5050'
	) h

ON
	a.SKU_CODE COLLATE DATABASE_DEFAULT = h.MATNR COLLATE DATABASE_DEFAULT

/* UPC, EAN */

LEFT JOIN

	(SELECT
		a.MATNR,
		a.EAN,
		b.UPC
	FROM
		(SELECT
			MATNR,
			EAN11 AS EAN
		FROM
			SAP_Master.dbo.MEAN
		WHERE
			EANTP = 'SI'
		) a
	LEFT JOIN
		(SELECT
			MATNR,
			EAN11 AS UPC
		FROM
			SAP_Master.dbo.MEAN
		WHERE
			EANTP = 'SZ'
		) b
	ON
		a.MATNR = b.MATNR
	) i

ON
	a.SKU_CODE COLLATE DATABASE_DEFAULT = i.MATNR COLLATE DATABASE_DEFAULT

/* set EAN from table MARA when we have no EAN in table MEAN */

UPDATE
	a
SET
	a.EAN = b.EAN11
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	SAP_Master.dbo.MARA b
ON
	a.SKU_CODE = b.MATNR
WHERE
	a.EAN IS NULL

/* -------- END color code, RID EMEA, Commodity codes, origin, EAN/UPC ----------- */

/* factory... */

UPDATE
	a
SET
	a.FACTORY = b.FACTORY
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	[dbo].[Plasma_Migration_Manual_Mapping] b
ON
	a.SKU_CODE = b.SKU_CODE

/* product type, article type, created season */

UPDATE
	a
SET
	a.PRODUCT_TYPE = b.[Product type],
	a.ARTICLE_TYPE = b.[Usage],
	a.CREATED_SEASON = b.[Collection/Season]
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	SAP_Master.dbo.MARA_ADDITIONAL b
ON
	a.SKU_CODE = b.Material

/* development type */

UPDATE
	[dbo].[Plasma_Migration_Data_v3]
SET
	[DEVELOPMENT_TYPE] = 'TN'
WHERE
	[CREATED_SEASON] = '201706'

UPDATE
	[dbo].[Plasma_Migration_Data_v3]
SET
	[DEVELOPMENT_TYPE] = 'CO'
WHERE
	[CREATED_SEASON] NOT IN ('201706')

/* black red... */

UPDATE
	[dbo].[Plasma_Migration_Data_v3]
SET
	COLOR_CODE = '23',
	COLOR_CODE2 = 'Q594'
WHERE
	SKU_CODE IN ('SS023157000','SS023158000')



;

/* ------ the tricky stuff with models/articles ----- */

USE
	[Plasma_Dev3]
GO

/* clean articles ... */

UPDATE
	a
SET
	a.PLANNING_ARTICLE = c.ZZ_ARTICLE,
	a.Comment = a.Comment + ', Clean articles'
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	SAP_Master.dbo.MARA b
ON
	a.SKU_CODE = b.MATNR
INNER JOIN
	(SELECT
		a.ZZ_ARTICLE,
		COUNT(a.MATNR) AS SKU_Count,
		SUM(CASE WHEN b.MAKTX LIKE '%#%' THEN 1 ELSE 0 END) AS Hashtag_count
	FROM
		SAP_Master.dbo.MARA a
	INNER JOIN
		SAP_Master.dbo.MAKT b
	ON
		a.MATNR = b.MATNR
	WHERE
		a.MTART = 'FERT'
	GROUP BY
		a.ZZ_ARTICLE
	) c
ON
	b.ZZ_ARTICLE = c.ZZ_ARTICLE
WHERE
	c.SKU_Count - c.Hashtag_count = 1
	/*c.SKU_Count = 1*/
	AND b.MTART = 'FERT'

/* price ref as article (no more testing...) */

UPDATE
	a
SET
	a.PLANNING_ARTICLE = b.[Pricing Ref  Matl],
	a.Comment = a.Comment + ', Price ref as article'
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN

	(SELECT
		a.*,
		b.[Material]
	FROM
		(SELECT
			a.[Pricing Ref  Matl],
			COUNT(a.[Material]) AS test
		FROM
			(SELECT DISTINCT
				a.[Pricing Ref  Matl],
				a.[Material]
			FROM
				SAP_Master.[dbo].[MVKE_priceref2] a
			INNER JOIN
				SAP_Master.dbo.MARA b
			ON
				a.Material = b.MATNR
			INNER JOIN
				SAP_Master.dbo.MARA_ADDITIONAL c
			ON
				a.Material = c.Material
			WHERE
				b.MTART = 'FERT'
				AND a.[Pricing Ref  Matl] IS NOT NULL
				AND LEFT(c.[CURRENT Season], 4) IN ('2017', '2018')
			) a
		GROUP BY
			a.[Pricing Ref  Matl]
		) a
	LEFT JOIN
		(SELECT DISTINCT
			a.[Pricing Ref  Matl],
			a.[Material]
		FROM
			SAP_Master.[dbo].[MVKE_priceref2] a
		INNER JOIN
			SAP_Master.dbo.MARA b
		ON
			a.Material = b.MATNR
		INNER JOIN
			SAP_Master.dbo.MARA_ADDITIONAL c
		ON
			a.Material = c.Material
		WHERE
			b.MTART = 'FERT'
			AND a.[Pricing Ref  Matl] IS NOT NULL
			AND LEFT(c.[CURRENT Season], 4) IN ('2017', '2018')
		) b
	ON
		a.[Pricing Ref  Matl] = b.[Pricing Ref  Matl]
	WHERE
		a.test = 1
		AND b.Material IS NOT NULL
	) b

ON
	a.SKU_CODE COLLATE DATABASE_DEFAULT = b.Material COLLATE DATABASE_DEFAULT

WHERE
	a.PLANNING_ARTICLE IS NULL
	AND a.PLANNING_MODEL IS NULL
	
	/*
	AND a.FAMILY IN
	('DIVE', 'Mechanical instrument', 'OUTDOOR', 'WATCHES', 'PERFORMANCE', 'ACC & Spare', 'SERVICE', 'MARKETING')
	*/


/* some articles from manual mapping */

UPDATE
	a
SET
	a.PLANNING_ARTICLE = b.PLANNING_ARTICLE,
	a.Comment = a.Comment + ', article from manual mapping'
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	[dbo].[Plasma_Migration_Manual_Mapping] b
ON
	a.SKU_CODE = b.SKU_CODE
WHERE
	a.PLANNING_ARTICLE IS NULL
	AND b.PLANNING_ARTICLE IS NOT NULL
	

/* clean planning models (using article assigment) */

UPDATE
	a
SET
	a.PLANNING_MODEL = b.ZZ_PLANMOD,
	a.Comment = CASE
		WHEN b.ZZ_PLANMOD IS NOT NULL THEN a.Comment + ', Clean models'
		ELSE a.Comment
	END
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	SAP_Master.dbo.MARA b
ON
	a.PLANNING_ARTICLE = b.MATNR
WHERE
	a.FAMILY IN
	('PERFORMANCE',
	'OUTDOOR',
	'DIVE',
	'WATCHES',
	'Mechanical instrument')
	AND a.Comment LIKE '%Clean articles%' /* test */

UPDATE
	a
SET
	a.PLANNING_MODEL = b.ZZ_PLANMOD,
	a.Comment = a.Comment + ', model for the rest'
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	(SELECT
		a.MATNR,
		b.ZZ_PLANMOD
	FROM
		SAP_Master.dbo.MARA a
	INNER JOIN
		SAP_Master.dbo.MARA b
	ON
		a.ZZ_ARTICLE = b.MATNR
	WHERE
		b.ZZ_PLANMOD IS NOT NULL
	) b
ON
	a.SKU_CODE = b.MATNR
WHERE
	a.PLANNING_MODEL IS NULL
	AND a.Model_comment IS NULL

/* the rest of planning models from manual mapping */

UPDATE
	a
SET
	a.PLANNING_MODEL = b.PLANNING_MODEL
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	[dbo].[Plasma_Migration_Manual_Mapping] b
ON
	a.SKU_CODE = b.SKU_CODE
WHERE
	a.PLANNING_MODEL IS NULL

USE
	[Plasma_Dev3]
GO

/* article/model/priceref description */

UPDATE
	a
SET
	a.ARTICLE_DESCRIPTION = b.MAKTX
FROM
	dbo.Plasma_Migration_Data_v3 a
INNER JOIN
	SAP_Master.dbo.MAKT b
ON
	a.PLANNING_ARTICLE COLLATE DATABASE_DEFAULT = b.MATNR COLLATE DATABASE_DEFAULT
WHERE
	b.SPRAS_ISO = 'EN'

UPDATE
	a
SET
	a.PLAN_MODEL_NAME = b.MAKTX
FROM
	dbo.Plasma_Migration_Data_v3 a
INNER JOIN
	SAP_Master.dbo.MAKT b
ON
	a.PLANNING_MODEL COLLATE DATABASE_DEFAULT = b.MATNR COLLATE DATABASE_DEFAULT
WHERE
	b.SPRAS_ISO = 'EN'

UPDATE
	a
SET
	a.ARTICLE_DESCRIPTION = b.[Material Description]
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	SAP_Master.dbo.MAKT_PRICEREF b
ON
	a.PLANNING_ARTICLE = b.Material

/* article description from SKU description for manually mapped articles */

UPDATE
	a
SET
	a.ARTICLE_DESCRIPTION = a.SKU_DESCRIPTION
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	[dbo].[Plasma_Migration_Manual_Mapping] b
ON
	a.SKU_CODE = b.SKU_CODE
	AND a.PLANNING_ARTICLE = b.PLANNING_ARTICLE
WHERE
	a.ARTICLE_DESCRIPTION IS NULL
	AND b.PLANNING_ARTICLE IS NOT NULL


/* ---------- project from manual mapping ------- */

UPDATE
	a
SET
	a.PROJECT = b.PROJECT
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	[dbo].[Plasma_Migration_Manual_Mapping] b
ON
	a.SKU_CODE = b.SKU_CODE

/* --------- end project --------- */



;

USE
	[Plasma_Dev3]
GO

/* factory cleanup */

UPDATE
	[dbo].[Plasma_Migration_Data_v3]
SET
	FACTORY = 'ASFIN2'
WHERE
	FACTORY IS NULL

/* master carton */

UPDATE
	a
SET
	a.MASTER_CARTON = b.MASTER_CARTON
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	[dbo].[Plasma_Migration_Manual_Mapping] b
ON
	a.SKU_CODE = b.SKU_CODE
WHERE
	a.MASTER_CARTON IS NULL


/* bags, cartons from BOM */

UPDATE
	a
SET
	a.[MASTER_CARTON] = b.[Komponentin kuvaus]
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	(SELECT DISTINCT
		[Komponentin kuvaus],
		[Nimike ylin taso]
	FROM
		[SAP_Master].[dbo].[SQ00_BOM]
	WHERE
		[Komponentin kuvaus] LIKE '%BOX%'
	) b
ON
	a.SKU_CODE = b.[Nimike ylin taso]
WHERE
	a.[MASTER_CARTON] IS NULL
	AND a.FAMILY NOT IN ('ACC & Spare', 'SERVICE', 'MARKETING')

UPDATE
	a
SET
	a.[MASTER_CARTON] = b.[Komponentin kuvaus]
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	(SELECT DISTINCT
		[Komponentin kuvaus],
		[Nimike ylin taso]
	FROM
		[SAP_Master].[dbo].[SQ00_BOM]
	WHERE
		[Komponentin kuvaus] LIKE '%BAG%'
		OR [Komponentin kuvaus] LIKE '%SUOJAPUSSI%'
		OR [Komponentin kuvaus] LIKE '%CLAMSHELL%'
	) b
ON
	a.SKU_CODE = b.[Nimike ylin taso]
WHERE
	a.[MASTER_CARTON] IS NULL
	AND a.FAMILY IN ('ACC & Spare', 'SERVICE', 'MARKETING')


UPDATE
	a
SET
	a.[MASTER_CARTON] = b.[Komponentin kuvaus]
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	(SELECT DISTINCT
		[Komponentin kuvaus],
		[Nimike ylin taso]
	FROM
		[SAP_Master].[dbo].[SQ00_BOM]
	WHERE
		[Komponentin kuvaus] LIKE '%BAG%'
	) b
ON
	a.SKU_CODE = b.[Nimike ylin taso]
WHERE
	a.[MASTER_CARTON] IS NULL

UPDATE
	[dbo].[Plasma_Migration_Data_v3]
SET
	MASTER_CARTON = 'DAYTON 126X90X92'
WHERE
	MASTER_CARTON IS NULL
	AND	PLAN_MODEL_NAME LIKE '%CORE%'
	AND FAMILY = 'OUTDOOR'

UPDATE
	[dbo].[Plasma_Migration_Data_v3]
SET
	RID_EMEA = '20170101'
WHERE
	RID_EMEA IS NULL

UPDATE
	[dbo].[Plasma_Migration_Data_v3]
SET
	[ARTICLE_DESCRIPTION] = [SKU_DESCRIPTION],
	Comment = Comment + ', article description from SKU'
WHERE
	[ARTICLE_DESCRIPTION] IS NULL
	AND [PLANNING_ARTICLE] LIKE '%SS%+%'

UPDATE
	[dbo].[Plasma_Migration_Data_v3]
SET
	Article_Comment = CASE
		WHEN Comment LIKE '%Clean articles%' THEN 'Clean articles'
		WHEN Comment LIKE '%Price ref as article%' AND Comment NOT LIKE '%string search%' THEN 'Price ref as article'
		WHEN Comment LIKE '%New price ref%' THEN 'New price ref'
		WHEN Comment LIKE '%priceref from SKU%' THEN 'priceref from SKU'
		WHEN Comment LIKE '%Product name for customized%' THEN 'Customized product'
		WHEN Comment LIKE '%update article product status in Plasma!%' THEN 'update article product status in Plasma!'
	END,
	Model_Comment = CASE
		WHEN Comment LIKE '%Clean Models%' THEN 'Clean Models'
		WHEN Comment LIKE '%Generated model%' THEN 'Generated model'
		WHEN Comment LIKE '%Model from article%' AND Comment NOT LIKE '%dirty%' THEN 'Model from article'
		WHEN Comment LIKE '%Model from article (dirty)%' THEN 'Model from article (dirty)'
		WHEN Comment LIKE '%Product name for customized%' THEN 'Customized product'
		WHEN Comment LIKE '%model from mapping%' THEN 'model from mapping'
		WHEN Comment LIKE '%model for the rest%' THEN 'model for the rest'
	END

/* CC_EU from Anu's file

UPDATE
	a
SET
	a.CC_EU = b.[EU Tariff code]
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	[dbo].[Customs_Cleanup] b
ON
	a.PLANNING_MODEL = b.[Planning code] */

/* carton temporary fix */

UPDATE
	a
SET
	a.MASTER_CARTON = b.MASTER_CARTON
FROM
	[dbo].[Plasma_Migration_Data_v3] a
LEFT JOIN
	(SELECT DISTINCT
		a.*,
		b.MASTER_CARTON
	FROM
		(SELECT DISTINCT
			[PLANNING_MODEL],
			COUNT([MASTER_CARTON]) AS test
		FROM
			[dbo].[Plasma_Migration_Data_v3]
		WHERE
			MASTER_CARTON IS NOT NULL
			AND PLANNING_MODEL IS NOT NULL
		GROUP BY
			PLANNING_MODEL
		) a
	LEFT JOIN
		[dbo].[Plasma_Migration_Data_v3] b
	ON
		a.PLANNING_MODEL = b.PLANNING_MODEL

	WHERE
		a.test = 1
		AND b.MASTER_CARTON IS NOT NULL
	) b
ON
	a.PLANNING_MODEL = b.PLANNING_MODEL
WHERE
	a.MASTER_CARTON IS NULL
	AND b.MASTER_CARTON IS NOT NULL

/* CC corrections

UPDATE
	a
SET
	a.CC_EU = b.CC_EU
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	[dbo].[CC_Corrections] b
ON
	a.SKU_CODE = b.SKU */

UPDATE
	[dbo].[Plasma_Migration_Data_v3]
SET
	PLANNING_ARTICLE = LEFT(SKU_CODE, 9) + '+',
	ARTICLE_DESCRIPTION = SKU_DESCRIPTION
WHERE
	ARTICLE_TYPE = 'CP'
	AND PLANNING_ARTICLE IS NULL

UPDATE
	a
SET
	a.CC_JP = b.CC_JP
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	[dbo].[Plasma_Migration_Manual_Mapping] b
ON
	a.SKU_CODE = b.SKU_CODE
WHERE
	b.CC_JP IS NOT NULL

/* carton, info from Eeva 

UPDATE
	a
SET
	a.[MASTER_CARTON] = b.PLASMA_CODE
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	[dbo].[Plasma_Migration_Carton2] b
ON
	a.SKU_CODE = b.SKU_CODE
WHERE
	a.[MASTER_CARTON] IS NULL */

/*
UPDATE
	[dbo].[Plasma_Migration_Data_v3]
SET
	MASTER_CARTON = 'SU40'
WHERE
	MASTER_CARTON IS NULL*/

UPDATE
	a
SET
	a.GROSS_WEIGHT = b.GROSS_WEIGHT
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	[dbo].[Plasma_Migration_Manual_Mapping] b
ON
	a.SKU_CODE = b.SKU_CODE

/* fix some long product names */

UPDATE
	[dbo].[Plasma_Migration_Data_v3]
SET
	PLAN_MODEL_NAME = CASE
		WHEN PLAN_MODEL_NAME LIKE '%SERVICE MODULE%' THEN REPLACE(PLAN_MODEL_NAME, 'SERVICE MODULE', 'SERV MOD')
		WHEN PLAN_MODEL_NAME LIKE '%WRIST HR%' THEN REPLACE(PLAN_MODEL_NAME, 'WRIST HR', 'WHR')
		ELSE PLAN_MODEL_NAME
	END
WHERE
	LEN(PLAN_MODEL_NAME) > 30

UPDATE
	[dbo].[Plasma_Migration_Data_v3]
SET
	General_Comment = CASE
		WHEN FAMILY IN ('ACC & Spare', 'MARKETING', 'SERVICE') THEN 'ACC+Spare, Marketing, Service'
		WHEN FAMILY IN ('PERFORMANCE', 'OUTDOOR', 'WATCHES', 'Mechanical instrument', 'DIVE') THEN 'BU products'
	END

UPDATE
	a
SET
	a.SKU_plan_relevance = b.[Planning relevance]
FROM
	Plasma_Dev3.dbo.Plasma_Migration_Data_v3 a
INNER JOIN
	SAP_Master.dbo.MARA_ADDITIONAL b
ON
	a.SKU_CODE = b.Material

UPDATE
	a
SET
	a.Article_plan_relevance = b.[Planning relevance]
FROM
	Plasma_Dev3.dbo.Plasma_Migration_Data_v3 a
INNER JOIN
	SAP_Master.dbo.MARA_ADDITIONAL b
ON
	a.PLANNING_ARTICLE = b.Material

UPDATE
	a
SET
	a.Model_plan_relevance = b.[Planning relevance]
FROM
	Plasma_Dev3.dbo.Plasma_Migration_Data_v3 a
INNER JOIN
	SAP_Master.dbo.MARA_ADDITIONAL b
ON
	a.PLANNING_MODEL = b.Material

UPDATE
	Plasma_Dev3.dbo.Plasma_Migration_Data_v3
SET
	Article_comment = 'Check!'
WHERE
	Article_comment IS NULL

UPDATE
	Plasma_Dev3.dbo.Plasma_Migration_Data_v3
SET
	Model_comment = 'Check!'
WHERE
	Model_comment IS NULL

UPDATE
	[dbo].[Plasma_Migration_Data_v3]
SET
	CC_US = '90141000'
WHERE
	SKU_CODE IN ('SS022518000', 'SS022519000')

/*
UPDATE
	a
SET
	a.PLANNING_MODEL = b.PLANNING_MODEL,
	a.PLAN_MODEL_NAME = c.MAKTX,
	a.Comment = a.Comment + ', model from mapping'
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	[dbo].[Plasma_Migration_Manual_Mapping] b
ON
	a.SKU_CODE = b.SKU_CODE
INNER JOIN
	SAP_Master.dbo.MAKT c
ON
	b.PLANNING_MODEL = c.MATNR
WHERE
	a.PLANNING_MODEL IS NULL
	AND b.PLANNING_MODEL IS NOT NULL */

UPDATE
	a
SET
	a.PROD_HIERARCHY = b.PROD_HIERARCHY
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	[dbo].[Plasma_Migration_Manual_Mapping] b
ON
	a.SKU_CODE = b.SKU_CODE
WHERE
	b.PROD_HIERARCHY IS NOT NULL

UPDATE
	[dbo].[Plasma_Migration_Data_v3]
SET
	Article_Comment = CASE
		WHEN Comment LIKE '%Clean articles%' THEN 'Clean articles'
		WHEN Comment LIKE '%Price ref as article%' AND Comment NOT LIKE '%string search%' THEN 'Price ref as article'
		WHEN Comment LIKE '%New price ref%' THEN 'New price ref'
		WHEN Comment LIKE '%priceref from SKU%' THEN 'priceref from SKU'
		WHEN Comment LIKE '%Product name for customized%' THEN 'Customized product'
		WHEN Comment LIKE '%update article product status in Plasma!%' THEN 'update article product status in Plasma!'
		WHEN Comment LIKE '%article from manual mapping%' THEN 'article from manual mapping'
	END,
	Model_Comment = CASE
		WHEN Comment LIKE '%Clean Models%' THEN 'Clean Models'
		WHEN Comment LIKE '%Generated model%' THEN 'Generated model'
		WHEN Comment LIKE '%Model from article%' AND Comment NOT LIKE '%dirty%' THEN 'Model from article'
		WHEN Comment LIKE '%Model from article (dirty)%' THEN 'Model from article (dirty)'
		WHEN Comment LIKE '%Product name for customized%' THEN 'Customized product'
		WHEN Comment LIKE '%model from mapping%' THEN 'model from mapping'
	END

UPDATE
	[dbo].[Plasma_Migration_Data_v3]
SET
	CC_CA = CC_EU,
	CC_US = CC_EU
WHERE
	LEN(CC_EU) = 10

UPDATE
	a
SET
	a.BRIEF = b.BRIEF
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	[dbo].[Plasma_Migration_Manual_Mapping] b
ON
	a.SKU_CODE = b.SKU_CODE
WHERE
	b.BRIEF IS NOT NULL

UPDATE
	a
SET
	a.[PROD_TYPE_DESCR] = b.[Description]
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	[SAP_Master].dbo.PROD_TYPE b
ON
	a.PRODUCT_TYPE = b.[ProductType]

UPDATE
	a
SET
	a.CUSTOMS_PRODTYPE = b.CUSTOMS_PRODTYPE
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	[dbo].[Plasma_Migration_Manual_Mapping] b
ON
	a.SKU_CODE = b.SKU_CODE

UPDATE
	a
SET
	a.PROJECT = a.PLAN_MODEL_NAME
FROM
	[dbo].[Plasma_Migration_Data_v3] a
WHERE
	a.PLANNING_MODEL IS NOT NULL

/* SRP prices */

UPDATE
	a
SET
	a.SRP_EMEA = b.[SRP EMEA],
	a.SRP_US = b.[SRP US],
	a.SRP_APAC = b.[SRP APAC]
FROM
	[dbo].[Plasma_Migration_Data_v3] a
INNER JOIN
	[SAP_Master].[dbo].[MARA_ADDITIONAL] b
ON
	a.SKU_CODE = b.Material

;